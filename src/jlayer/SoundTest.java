package jlayer;

import java.io.*;

import javazoom.jl.player.*;

/**
 * JLayerによるもの。mp3のみのサポート？？？
 * @author ysugimura
 */
public class SoundTest {

  public static void main(String[]args) {
    try (FileInputStream fis = new FileInputStream("sound/select.mp3")) {
      Player player = new Player(fis);
      player.play();
      // 再生が終了するまでここには来ない。
      System.out.println("" + player.isComplete());
      
    }  catch(Exception e){
      System.out.println(e);
    }
  }
}
