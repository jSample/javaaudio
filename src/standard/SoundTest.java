package standard;

import java.io.*;

import javax.sound.sampled.*;

/**
 * Java標準のAPIによるオーディオファイルの再生。
 * .WAVしかサポートしていないらしい。
 * @author ysugimura
 */
public class SoundTest {

  public static void main(String[] args) {
    try (AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("sound/select.wav"))) {
      Clip clip = AudioSystem.getClip();
      clip.open(audioInputStream);
      clip.start();
      // 再生が終了しなくともここに到達する
      System.out.println("finished");
      Thread.sleep(5000);
    } catch (Exception ex) {
      System.out.println("Error with playing sound.");
      ex.printStackTrace();
    }
  }
}
