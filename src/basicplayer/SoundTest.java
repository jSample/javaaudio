package basicplayer;

import java.io.*;
import java.net.*;

import javazoom.jlgui.basicplayer.*;

/**
 * BasicPlayerによるもの。必要な関連ライブラリが多すぎるかもしれない。
 * @author ysugimura
 *
 */
public class SoundTest {

  public static void main(String[]args) {
    File file = new File("sound/select.mp3");
    BasicPlayer player = new BasicPlayer();
    try {
        player.open(file.toURL());
        player.play();
        // 再生が終了しなくともここに到達する。
        System.out.println("finished");
    } catch (BasicPlayerException | MalformedURLException e) {
        e.printStackTrace();
    }
  }
}
