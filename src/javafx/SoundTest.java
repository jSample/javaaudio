package javafx;

import java.io.*;

import javafx.application.*;
import javafx.scene.media.*;
import javafx.stage.*;

/**
 * JavaFXの場合、JavaFXのフレームワークを初期化しないと使えない。
 * mp3の再生はできる。
 * @author ysugimura
 *
 */
public class SoundTest extends Application {

  @Override
  public void start(Stage stage) throws Exception {
    Media hit = new Media(new File("sound/select.mp3").toURI().toString());
    MediaPlayer mediaPlayer = new MediaPlayer(hit);
    mediaPlayer.play();
    // 再生が終了しなくてもここに到達する
    System.out.println("finished");
  }
  
  public static void main(String[]args) {
    Application.launch(SoundTest.class, args);
  }
}
